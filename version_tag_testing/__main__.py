from importlib.metadata import version

print('Retrieving version from package')
v = version('version_tag_testing')

print(f"Found version: {v}")

